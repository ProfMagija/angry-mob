#include "../include/Itamae.h"

#ifdef _WIN32
#define PLATFORM_SEND(a,b,c) send(a,static_cast<const char *>(b),c,0)
#else
#define PLATFORM_SEND(a,b,c) write(a,b,c)
#endif // _WIN32

#ifdef _WIN32
#define PLATFORM_RECV(a,b,c) recv(a,b,c,0)
#else
#define PLATFORM_RECV(a,b,c) read(a,b,c)
#endif // _WIN32



namespace Itamae
{
    int sock;
    Cell board[64][64];
    Location my_base, their_base;
    int ni, nj;
    int myUnitsTotal, theirUnitsTotal;
    int deadUnitsCount;
    int neutralUnitsCount;

    bool transmit(const void * data, int len)
    {
//      printf("sending %d bytes...", len);
        if (PLATFORM_SEND(sock, data, len) == len)
        {
//          printf("sent!\n");
            return true;
        }
        else
        {
            printf("failed/partial transmit\n");
            return false;
        }
    }

    char data[1024];

    bool sendSubmove(Move mvs)
    {
        data[0] = (char)(mvs.size() / 256);
        data[1] = (char)(mvs.size() % 256);
        for (size_t i = 0; i < mvs.size(); i++)
        {
            data[2 + 5*i] = (char)mvs[i].i;
            data[3 + 5*i] = (char)mvs[i].j;
            data[4 + 5*i] = (char)(mvs[i].k / 256);
            data[5 + 5*i] = (char)(mvs[i].k % 256);
            data[6 + 5*i] = (char)mvs[i].d;
        }
        return transmit(data, 5 * mvs.size() + 2);
    }

    char readByte()
    {
        //printf("reading byte...");
        char t;
        if (PLATFORM_RECV(sock, &t, 1) == 1)
        {
        //    printf("read!\n");
            return t;
        }
        else
        {
        //   printf("failed!\n");
            exit(5); // server closed connection, exit
        }
    }

    Submove readSubmove()
    {
        char t[5];
        //printf("reading a Submove...");
        if (PLATFORM_RECV(sock, t, 5) != 5)
        {
        //    printf("failed\n");
            exit(6); // server closed connection, exit
        }
        //printf("done\n");
        return {t[0], t[1], t[2] * 256 + t[3], t[4]};
    }

    bool loadBoard()
    {
        ni = readByte();
        nj = readByte();
        for (int i = 0; i < ni; i++)
        {
            for (int j = 0; j < nj; j++)
            {
                int c = readByte();
                if (c > 127) c -= 256;
                if (c == MY_BASE)
                {
                    board[i][j].base = 0;
                    board[i][j].myUnitCount = 1;
                    my_base = {i, j};
                }
                else if (c == THEIR_BASE)
                {
                    board[i][j].base = 1;
                    board[i][j].theirUnitCount = 1;
                    their_base = {i, j};
                }
                else if (c == BLOCK)
                {
                    board[i][j].base = -1;
                    board[i][j].block = true;
                }
                else if (c >= 0)
                {
                    board[i][j].neutralUnitCount = c;
                    board[i][j].base = -1;
                    neutralUnitsCount += c;
                }
                else
                {
                    printf("invalid data recived %d", c);
                    return false;
                }
            }
        }
        deadUnitsCount = 0;
        myUnitsTotal = 1;
        theirUnitsTotal = 1;
        return true;
    }

    // jeste memmory leak, ali 20 bajtova nikoga nece ubiti
    char * stringAsData(const char * str)
    {
        char * buf = new char[strlen(str) + 1];
        buf[0] = (char)strlen(str);
        for (size_t i = 0; i < strlen(str); i++)
        {
            buf[1 + i] = str[i];
        }
        return buf;
    }

    bool init(const char * name, const char * addr, const char * port)
    {
#ifdef WIN32
        WSAData d;
        int krj;
        if ((krj = WSAStartup(MAKEWORD(2,2),&d)) != 0)
        {
            printf("WSAStartup Failed! %i\n", krj);
            return false;
        }
#endif // WIN32
        addrinfo hints, *result, *r;
        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        int s = getaddrinfo(addr, port, &hints, &result);
        if (s != 0)
        {
            printf("getaddrinfo returned error: %s\n", gai_strerror(s));
            return false;
        }

        for (r = result; r != NULL; r = r->ai_next)
        {
            sock = socket(r->ai_family, r->ai_socktype, r->ai_protocol);
            if (sock == -1) continue;
            if (connect(sock, r->ai_addr, r->ai_addrlen) != -1)
                break;
            close(sock);
        }

        if (r == NULL)
        {
            printf("Could not connect to %s\n", addr);
            return false;
        }

        if (transmit(stringAsData(name), strlen(name) + 1))
            return loadBoard();
        else
            return false;
    }

    Cell getCell(int x, int y)
    {
        if (x < 0 || y < 0 || x >= ni || y >= nj)
            return {0, 0, 0, -1, true};
        return board[x][y];
    }

    void applySubmoveMy(Submove m)
    {
        board[m.i][m.j].myUnitCount -= m.k;
        board[m.i + dx[m.d]][m.j + dy[m.d]].myUnitCount += m.k;
    }

    void applySubmoveTheir(Submove m)
    {
        board[m.i][m.j].theirUnitCount -= m.k;
        board[m.i + dx[m.d]][m.j + dy[m.d]].theirUnitCount += m.k;
    }

    void applySubmoves(Move &my, Move &their)
    {
        for (size_t i = 0; i < my.size(); ++i)
            applySubmoveMy(my[i]);
        for (size_t i = 0; i < their.size(); ++i)
            applySubmoveTheir(their[i]);

        for (int i = 0; i < ni; i++)
        {
            for (int j = 0; j < nj; j++)
            {
                int m = std::min(board[i][j].myUnitCount, board[i][j].theirUnitCount);
                if (m == 0) continue;
                board[i][j].myUnitCount -= m;
                board[i][j].theirUnitCount -= m;
                myUnitsTotal -= m;
                theirUnitsTotal -= m;
                deadUnitsCount += 2 * m;
            }
        }

        if (myUnitsTotal <= MAX_UNITS)
        {
            for (int i = 0; i < ni; i++)
            {
                for (int j = 0; j < nj; j++)
                {
                    if (board[i][j].myUnitCount > 0)
                    {
                        board[i][j].myUnitCount += board[i][j].neutralUnitCount;
                        myUnitsTotal += board[i][j].neutralUnitCount;
                        board[i][j].neutralUnitCount = 0;
                    }
                }
            }
        }

        if (theirUnitsTotal <= MAX_UNITS)
        {
            for (int i = 0; i < ni; i++)
            {
                for (int j = 0; j < nj; j++)
                {
                    if (board[i][j].theirUnitCount > 0)
                    {
                        board[i][j].theirUnitCount += board[i][j].neutralUnitCount;
                        theirUnitsTotal += board[i][j].neutralUnitCount;
                        board[i][j].neutralUnitCount = 0;
                    }
                }
            }
        }
    }

    void play(Move &my, Move& their)
    {
        if (!sendSubmove(my))
            exit(0);
        int n = readByte() * 256 + readByte();
        their.clear();
        for (int i = 0; i < n; i++)
        {
            their.push_back(readSubmove());
        }
        applySubmoves(my, their);
    }
}
