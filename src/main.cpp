#include <iostream>
#include "../include/Itamae.h"
#include "../include/Wakiita.h"

using namespace std;

int main()
{
    if (!Itamae::init(Wakiita::getName(), "localhost", "58351")) return 7;
    Wakiita::init();
    Itamae::Move my, their;
    for(;;)
    {
        my.clear();
        Wakiita::makeMove(their, my);
        their.clear();
        Itamae::play(my, their);
    }
}
