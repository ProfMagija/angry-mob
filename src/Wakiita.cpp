#include "../include/Wakiita.h"

using namespace Itamae;

namespace Wakiita
{
    void init()
    {
        srand(time(NULL));
        //sleep(2); // malo da se odmori
    }

    const char * getName()
    {
        return "Profesor Random";
    }

    bool isValidSubmove(int i, int j, int k, int d)
    {
        return getCell(i,j).myUnitCount >= k && k > 0 &&
            3 >= d && d >= 0 &&
            !getCell(i+dx[d], j+dy[d]).block;
    }

    int mov = 0;

    void makeMove(Itamae::Move &opponentMove, Itamae::Move &myMove)
    {
        mov++;
        myMove.clear();
        for (int i = 0; i < ni; i++)
        {
            for (int j = 0; j < nj; j++)
            {
                for (int k = 0; k < getCell(i,j).myUnitCount; k++)
                {
                    int d = rand() % 4;
                    while (!isValidSubmove(i, j, 1, d)) d = rand()%4;
                    myMove.push_back({i, j, 1, d});
                }
            }
        }

        printf("Move %i\n", mov);

        for (size_t i = 0; i < myMove.size(); i++)
            printf("    %i %i %i %i\n", myMove[i].i, myMove[i].j, myMove[i].k, myMove[i].d);

        fflush(stdout);
    }
}
