#ifndef ITAMAE_H
#define ITAMAE_H

#ifdef _WIN32
#define _WIN32_WINNT 0x501
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#endif

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <algorithm>
#include <vector>

namespace Itamae
{


    const int dx[] = {0,-1,0,1};
    const int dy[] = {1,0,-1,0};

    // zar je ovaj docstring zaista potreban?
    /// struktura koja predstavlja lokaciju (x,y)
    struct Location
    {
        int x, y;
    };

    /// struktura koja predstavlja jedan pokret, po
    /// specifikacijama
    struct Submove
    {
        int i, j;
        int k, d;
    };

    typedef std::vector<Submove> Move;

    /// Struktura koja predstavlja polje celije
    struct Cell
    {
        /// broj igracevih jedinica na polju
        char myUnitCount;
        /// broj protivnickih celija na polju
        char theirUnitCount;
        /// broj neutralaca na polju
        char neutralUnitCount;
        /// predstavlja polje bazu:
        ///
        /// * `-1` polje nije baza
        /// *  `0` polje je igraceva baza
        /// *  `1` polje je protivnicka baza
        char base;
        /// `true` ako je celija neprohodna (blokirana)
        bool block;
    };

    const int MY_BASE = -1;
    const int THEIR_BASE = -2;
    const int BLOCK = -3;

    const int MAX_UNITS = 8;

    bool init(const char * name, const char * server, const char * port);

    /// Vraca celiju na polju (x,y) table, ili blokirano polje
    /// ukoliko se nalazi van table
    Cell getCell(int x, int y);

    void play(Move &my, Move &their);

    extern int sock;

    extern Cell board[64][64];
    extern Location my_base, their_base;
    extern int ni, nj;

    /// broj igracevih i protivnickih jedinica
    extern int myUnitsTotal, theirUnitsTotal;
    /// broj do sada poginulih jedinica, i igracevih
    /// i protivnickih, dakle uvek paran
    extern int deadUnitsCount;
    /// broj preostalih neutralaca
    extern int neutralUnitsCount;
}

#endif // ITAMAE_H
