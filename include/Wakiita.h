#ifndef WAKIITA_H
#define WAKIITA_H

#include <vector>
#include <ctime>

#include "Itamae.h"

namespace Wakiita
{
    void init();
    const char * getName();
    void makeMove(Itamae::Move &opponentMove, Itamae::Move &myMove);
}

#endif // WAKIITA_H_
