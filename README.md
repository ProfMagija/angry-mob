Angry Mob TCP/IP Abstraction Layer (Itamae)
===========================================

A (somewhat) simple and fast abstraction and simplification **"Angry Mob"**
wrapper, with no additional dependencies.

How To Use
----------

Just edit the `src/Wakiita.cpp` file. Your job is to implement the following
three functions (declared in `include/Wakiita.h`):

* `void init()`: Used for any initialisation jobs you need to do (called only
once, after map has been fetched).
* `const char * getName()`: Just return the name of the program.
* `void playMove(Move &opponent, Move &my)`: Preform one turn. Parameters
passed represents the moves opponent made last turn, and a vector to fill in
your own moves. On first move, parameter `opponent` is an empty vector.

The "API" of the wrapper is very simple and bare:

* `Cell getCell(int x, int y)`: Gets the cell at coordinates (x,y). If outside
the bounds of the map returns a block.
* `int dx[], dy[]`: The *delta x* and *delta y* arrays for given directions.
* `int ni, nj`: Size of the board in `i` and `j` directions
* `int myUnitsTotal`: Total number of your units available.
* `int theirUnitsTotal`: Total number of opponent units.
* `int neutralUnitsCount`: Total number of neutral units left
* `int deadUnitsCount`: Total number of killed units.
* `Location my_base, their_base`: Locations of players and opponents base

Other functions and variables shouldn't be of great use to the player.

Structures
----------

* `Submove`: Just as in the game specification, represents moving `k` units from 
location `(i,j)` in direction `d`.
* `Move`: Alias for vector<Submove> (could've been implemented on it's own, but
it was easier this way, and would've contained a vector anyway)
* `Location`: Used just once, but let it be.
* `Cell`: One cell on the board
   * `char myUnitCount`: Number of player's units on that cell
   * `char theirUnitCount`: Number of opponent's units
   * `char neutralUnitCount`: Number of neutral units
   * `char base`: Who's base is it (`-1`-not a base, `0`-my, `1`-opponent's)
   * `bool block`: true if it's not passable

Naming of things
----------------

Arbitrary
